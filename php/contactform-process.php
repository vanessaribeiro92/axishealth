<?php
$errorMSG = "";

if (empty($_POST["name"])) {
    $errorMSG = "Nome é obrigatório ";
} else {
    $name = $_POST["name"];
}

if (empty($_POST["telephone"])) {
    $errorMSG = "Telefone é obrigatório ";
} else {
    $telephone = $_POST["telephone"];
}

if (empty($_POST["email"])) {
    $errorMSG = "Email é é obrigatório";
} else {
    $email = $_POST["email"];
}

if (empty($_POST["company"])) {
    $errorMSG = "Empresa é obrigatório";
} else {
    $company = $_POST["company"];
}

if (empty($_POST["message"])) {
    $errorMSG = "Mensagem é obrigatório ";
} else {
    $message = $_POST["message"];
}

// if (empty($_POST["terms"])) {
//     $errorMSG = "Terms is required ";
// } else {
//     $terms = $_POST["terms"];
// }

$EmailTo = "your-email@domain.com";
$Subject = "New message from AxisHealth page";

// prepare email body text
$Body = "";
$Body .= "Name: ";
$Body .= $name;
$Body .= "\n";
$Body .= "Telephone: ";
$Body .= $telephone;
$Body .= "\n";
$Body .= "Email: ";
$Body .= $email;
$Body .= "\n";
$Body .= "Company: ";
$Body .= $company;
$Body .= "\n";
$Body .= "Message: ";
$Body .= $message;
$Body .= "\n";
// $Body .= "Terms: ";
// $Body .= $terms;
// $Body .= "\n";

// send email
$success = mail($EmailTo, $Subject, $Body, "From:".$email);

// redirect to success page
if ($success && $errorMSG == ""){
   echo "sucesso";
}else{
    if($errorMSG == ""){
        echo "Algo deu errado :(";
    } else {
        echo $errorMSG;
    }
}
?>